﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Practica_Basica_6
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Introuccion Array Irregulares
            int [] [] irregular =new int[3][];
            // Contiene 5 valores
            irregular[0] =new int[5];
            // Contiene 3 valores
            irregular[1] =new int[3];
            // Contiene 2 valores
            irregular[2] = new int[2];

            // Asignar valores
            irregular[0] = new int []{2,3,8,9,11};
            irregular[1] = new int []{2,70,9};
            irregular[2] = new int []{18,4};

            // otra forma de crear irregulares
            int [][] irregular2 = new int [][]{
                 new int []{2,3,8,9,11},
                 new int []{2,70,9},
                 new int []{18,4}

            };

            System.Console.WriteLine("El valor en el medio del primer array es {0}", irregular2[0][2]);
            // Loop For que me permita ver todos los valores del array 
           
            for (int i = 0; i < irregular2.Length; i++)
            {
                System.Console.WriteLine(" Los valores el Array: {0}",i);
                   for (int j = 0; j < irregular2[i].Length; j++)
                   {

                       System.Console.WriteLine( irregular2[i][j]);
                   }
            }
            #endregion
            
            #region Desafio Cumpleaños

            string[][] invitados = new string[][]{
                new string[]{"Miguel","Joaquin","Maria"},
                new string[]{"Roberto","Daniela"},
                new string[]{"Diego","Javiera","Carmen","Ana","Namco"}
            };

            System.Console.WriteLine( "Hola {0} Soy {1}", invitados[0][0] , invitados[1][0]);

            #endregion

            #region Array como Parámetros

            int [] calificaciones = new int[] {8,6,7,8,1,3};
            double promedioResultado = ObtenerPromedio(calificaciones);
            foreach (int nota  in calificaciones)
            {
                System.Console.WriteLine("{0}", nota);
            }
            System.Console.WriteLine("El promedio es {0}", promedioResultado);

            #endregion

            #region Desafio Sueldos

            int[] sueldoClientes = new int[]{ 100000,20000,30000};
            int[] nuevosSueldos;
            
            nuevosSueldos =  credito(sueldoClientes);

            foreach (int sueldos in nuevosSueldos)
            {
                System.Console.WriteLine(sueldos);
            }
                
            #endregion           

            #region Array list

                // Los array list nos permite contener diferentes tipos de objetos (int, double, float, string)

                // Declarar un ArrayList Indeternminado
                ArrayList miArraylist = new ArrayList();

                // Declarar un ArrayList Determinado 
                ArrayList miArraylist2 = new ArrayList(100);

                // Agregar elementos
                miArraylist.Add(25);
                miArraylist.Add("Hola");
                miArraylist.Add(25.5);
                miArraylist.Add(25.5);
                miArraylist.Add(25);
                miArraylist.Add(5);
                miArraylist.Add("Adios");
                miArraylist.Add("Buenos dias");


                // Eliminar elementos del arraylist por valor
                miArraylist.Remove(25);


                // Eliminar elementos del arraylist por indice
                miArraylist.RemoveAt(0);

                // Contar elementos de array list

                System.Console.WriteLine(miArraylist.Count);  

                double suma = 0;
                foreach (object item in miArraylist)
                {
                    if( item is int)
                    {
                        suma+= Convert.ToDouble(item);
                    }
                    else if ( item is double){
                        suma += (double)item; 
                    }
                    else if ( item is string){
                        System.Console.WriteLine(item);
                    }

                }

                System.Console.WriteLine(suma);
            #endregion

            #region Listas
                
                // Pueden aumentar o disminuir dinamicamente
                // Una coleccion e suna clase, y requiere una instancia de la clase antes de comenzar a agregar elementos en esa coleccion

                // Lista de tipo int sin valores
                var numeros = new List<int>();

                // Lista de tipo int con valores
                var numeros2 = new List<int>{1,5,35,100};


            #endregion
      
            #region  Array vs ArrayList vs List

                //Inmutable -  Limitado al 1 tipo     
                int[] puntajes = new int[]{ 99,2,3,51};

                // Mutable 
                List<int> lista = new List<int>{1,2,3,4,5};
                // Agregamos elementos
                lista.Add(0);
                lista.Add(2);
                lista.Add(4);
                // Ordenamos de mayor a menor
                lista.Sort();
                // Metodos
                // A partir del indice 2 elimina 2 elementos
                lista.RemoveRange(2,2);


                foreach (var item in lista)
                {
                    System.Console.WriteLine(item);
                }

                // Devuelve un Booleano 
                System.Console.WriteLine(lista.Contains(4));

                // Busca por el indice con expresion lambda 
                int indice = lista.FindIndex( x => x == 4);
                System.Console.WriteLine(lista[indice]);

                // Eliminar
                lista.RemoveAt(indice);

                lista.ForEach( i => System.Console.WriteLine(i));


                ArrayList arr1 = new ArrayList();
                arr1.Add(1);
                arr1.Add("dos");
                arr1.Add(3.0);
                arr1.Add("4");
                arr1.Add( new numero { n = 4} );

                foreach (var item in arr1)
                {
                    System.Console.WriteLine(item);
                }

            #endregion
      
        }

        public static double ObtenerPromedio(int[] arrayPuntajes){
            int cantidad = arrayPuntajes.Length;
            double promedio;
            int suma = 0;

            for (int i = 0; i < cantidad ; i++)
            {
                suma += arrayPuntajes[i];
            }
            promedio = (double) suma / cantidad;

            return promedio;
        }

        public static int[] credito(int[] sueldoClientes){

            int creditoDolares = 200;
            int total;
            int sueldoActual;

            for (int i = 0; i < sueldoClientes.Length ; i++)
            {
                sueldoActual = sueldoClientes[i];
                total = sueldoClientes[i] + creditoDolares;
                sueldoClientes[i] = total;
                System.Console.WriteLine("El cliente:{0} Sueldo:{1} Credito:{2} NuevoSueldo:{3}",
                 i, sueldoActual, creditoDolares, total);
            }
            return sueldoClientes;

        }
        
    }

    class numero{

        public int n { get; set; }

        public override string ToString(){
            return n.ToString();
        }

    }
}

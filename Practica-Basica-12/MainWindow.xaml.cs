﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Practica_Basica_12
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
           InitializeComponent();

            #region Codigo de Controles iniciales
            /*
           Grid grid = new Grid();
           this.Content = grid;
           Button btn = new Button();
           btn.FontSize = 26;

           WrapPanel wrapPanel = new WrapPanel();
           TextBlock textBlock = new TextBlock();

           textBlock = new TextBlock();
           textBlock.Text = "Boton";
           textBlock.Foreground = Brushes.Blue;
           wrapPanel.Children.Add(textBlock);

           textBlock = new TextBlock();
           textBlock.Text = "Multi";
           textBlock.Foreground = Brushes.Red;
           wrapPanel.Children.Add(textBlock);

           textBlock = new TextBlock();
           textBlock.Text = "Color";
           textBlock.Foreground = Brushes.Green;
           wrapPanel.Children.Add(textBlock);

           btn.Content = wrapPanel;
           grid.Children.Add(btn);  
            
            */
                
            #endregion

        }

        private void Button_Click(object sender, RoutedEventArgs e){

             MessageBox.Show("El boton a sido presionado - Direct Event ");
        }

        private void Button_MouseUp(object sender, RoutedEventArgs e){

             MessageBox.Show("El boton a sido Soltado - Bubbling Event ");
        }
        private void Button_PreviewMouseUp(object sender, RoutedEventArgs e){

             MessageBox.Show("El boton a sido Soltado - Tunneling Event ");
        }

        


        
    }
}
 
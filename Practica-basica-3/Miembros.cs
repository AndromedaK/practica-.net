using System;
using System.Diagnostics;

namespace Practica_Basica_3{

    class Miembros{

        // Miembros - Campos privados
        private string nombrePersona;
        private string tituloTrabajo;
        private decimal salario;

        // Miembros - Campos públicos
        public int edad; 

        // Miembros - propiedades
        public string TituloTrabajo{
        get
         {
               return tituloTrabajo; 
         } 
         set{
             tituloTrabajo = value;
         }}

        // Miembros - Metodos privado

        private void CompartirInfoPrivada(){

            Console.WriteLine("Hola, mi salario es {0}", salario);
        }

        // Miembros - Métodos Publicos

        public void Amigo(bool esAmigo){
            if (esAmigo)
            {
                CompartirInfoPrivada();
            }
            else
            {
                Console.WriteLine("Hola, mi nombre es {0}, y mi edad es {1}", nombrePersona, edad);
            }

        }

        // Miembros - Constructor
        public Miembros()
        {
            edad = 30;
            salario = 500000;
            nombrePersona = "Ana";
            Console.WriteLine("Objeto Creado");
        }

        // Miembros - Destructores

        ~Miembros(){
            //Limpiar declaraciones
            Console.WriteLine("Destruccione de objeto miembro");
            Debug.Write("Destruccion de objeto miembro");
        }
    }

}
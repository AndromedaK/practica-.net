﻿using System;

namespace Practica_Basica_3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Crea un objeto desde mi clase
            Humanos Mujer = new Humanos("Luly","Love","rojo", 21);
            // acceder a una variable publica externa 
            // Mujer.primerNombre = "LulyLove";
            // llama a un metodo de la clase 
            Mujer.Presentarme();

            // Crea un nuebo objeto de mi clase
            Humanos Hombre = new Humanos("Felipe","Camiroaga","azul",49);
            // accesde a una variable publica
            //Hombre.primerNombre = "Camiroaga";
            // Llama a un metodo de la clase
            Hombre.Presentarme();

            Humanos humanoBasico = new Humanos();
            humanoBasico.Presentarme();

            Humanos humanoSinEdad = new Humanos("Sebastiam","Piñera",65);
            humanoSinEdad.Presentarme();

            Caja caja = new Caja(3,3,10);
            caja.MuestraInfo();

            Miembros nuevoMiembro = new Miembros();
            nuevoMiembro.Amigo(true);

            // Presiona una tecla
            //Console.ReadKey();
        }
    }
}

using System;

namespace Practica_Basica_7
{
    public class Publicacion{

        
        private static int publicacionID;
        protected int PublicacionID
        {
            get { return publicacionID; }
            set { publicacionID = value; }
        }

        protected string Titulo { get; set; }       
        protected string Autor  { get; set; }             
        protected bool EsPublic { get; set; }             
             

        // Constructor por defecto
        public Publicacion()
        {
            PublicacionID = CrearId();
            Titulo        = "Mi primer publicacion";
            Autor         = "Cristopher Angulo";
            EsPublic      = true;
        }
        
        // Constructor parametrizado 
        public Publicacion(string titulo, bool esPublico, string autor)
        {
            this.PublicacionID = CrearId();
            this.Autor         = autor;
            this.EsPublic      = esPublico;
            this.Titulo        = titulo;
        }

        // Metodo para crear ID 
        protected int CrearId(){

            return ++publicacionID; 
        }

        // Metodo para editar publicacion 

        public void Editar(string titulo, bool esPublico){

            this.Titulo   = titulo;
            this.EsPublic = esPublico;
        }

        
        public override string ToString(){
            return String.Format(" {0} - {1} - CREADO POR {2}", this.PublicacionID, this.Titulo, this.Autor);
        }





    }
    
}
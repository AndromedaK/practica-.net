using System;
using System.Threading;

namespace Practica_Basica_7
{
    public class Video: Publicacion
    {

        // variables

        protected bool seReproduce = false;
        protected int duracionActual = 0;
        Timer reloj;


        public string UrlVideo { get; set; }

        public int Duracion { get; set; }

        public Video()
        {
            
        }

        public Video(string autor, string titulo, bool esPublico, string urlVideo, int duracion)
        {
            this.PublicacionID  = CrearId();
            this.Autor          = autor;
            this.Titulo         = titulo;
            this.EsPublic       = esPublico;
            this.UrlVideo       = urlVideo;
            this.Duracion       = duracion;
        }

        public override string ToString(){
            return String.Format(" {0} - {1} - CREADO POR {2} - {3} - {4}", this.PublicacionID, this.Titulo, this.Autor, this.UrlVideo, this.Duracion);

        }

        public void Play(){

            if(!seReproduce){
                seReproduce = true; 
                System.Console.WriteLine("Reproduciendo");
                reloj = new Timer( Reproduccion, null, 0, 1000);
            }

        }

        private void Reproduccion(object state)
        {
            if (duracionActual < Duracion)
            {
                duracionActual++;
                System.Console.WriteLine("Video en {0}",  duracionActual);
                GC.Collect();
            }
            else{
                Stop();
            }
        }

        public void Stop(){

            if (seReproduce)
            {
                seReproduce = false;
                System.Console.WriteLine("Detenido en {0}", duracionActual);
                duracionActual = 0;
                reloj.Dispose();                
            }

        }



    }
    
}
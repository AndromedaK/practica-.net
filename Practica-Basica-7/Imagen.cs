using System;

namespace Practica_Basica_7{

    public class Imagen:Publicacion
    {

        // propiedad nueva
        public string UrlImagen { get; set; }

        // Constructor por defecto
        public Imagen(){}

        // Constructor parametrizado
        public Imagen(string titulo, string autor, string urlImagen, bool esPublico)
        {
            this.PublicacionID = CrearId();
            this.Titulo        = titulo;
            this.Autor         = autor;
            this.EsPublic      = esPublico;
            this.UrlImagen     = urlImagen;
        }

        public override string ToString(){

            return String.Format(" {0} - {1} - CREADO POR {2} - {3}", this.PublicacionID, this.Titulo, this.Autor, this.UrlImagen);
        }
    }
}
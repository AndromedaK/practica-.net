﻿using System;

namespace Practica_Basica_7
{
    class Program
    {
        static void Main(string[] args)
        {
            Publicacion post1 = new Publicacion( "Gracias por los saludos!!!", true,"Cristopher");
            System.Console.WriteLine(post1.ToString());
        
            Imagen imagen1 = new Imagen("Vacaciones en Familia", "Cristopher","https://imagen.com/familia",true);
            System.Console.WriteLine(imagen1.ToString());
        
            Video video1 = new Video("Cristopher"," Video Mi perro Jugando", false, "https://youtube.com/mi-perro", 5 );
            video1.Play();
            System.Console.WriteLine("Presione cualquier tecla para detener");
            Console.ReadKey();
            video1.Stop();
            System.Console.WriteLine(video1.ToString());




        }
    }
}

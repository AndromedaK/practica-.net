﻿using System;

namespace Practica_Basica_5
{
    class Program
    {
        static void Main(string[] args)
        {

            int jugador = 2;
            int ingreso = 0;
            bool ingresoCorrecto = true;

            do
            {
                if (jugador == 2) {
                    
                    PonerXoO(jugador, ingreso); 
                    jugador = 1;
                }
                else if (jugador == 1){
                    PonerXoO(jugador, ingreso); 
                    jugador = 2;

                }
                CrearTablero();

                #region Verificar Ganador

                char[] cadaSigno = { 'X', 'O'};
                foreach (var signo in cadaSigno)
                {
                    if (    (tableroJuego[0,0] == signo ) &&  (tableroJuego[0,1] == signo ) && (tableroJuego[0,2] == signo )
                         || (tableroJuego[1,0] == signo ) &&  (tableroJuego[1,1] == signo ) && (tableroJuego[1,2] == signo)
                         || (tableroJuego[2,0] == signo ) &&  (tableroJuego[2,1] == signo ) && (tableroJuego[2,2] == signo)
                         || (tableroJuego[0,0] == signo ) &&  (tableroJuego[1,0] == signo ) && (tableroJuego[2,0] == signo)
                         || (tableroJuego[0,1] == signo ) &&  (tableroJuego[1,1] == signo ) && (tableroJuego[2,1] == signo)
                         || (tableroJuego[0,2] == signo ) &&  (tableroJuego[0,1] == signo ) && (tableroJuego[0,2] == signo)
                         || (tableroJuego[0,0] == signo ) &&  (tableroJuego[1,1] == signo ) && (tableroJuego[2,2] == signo)
                         || (tableroJuego[0,2] == signo ) &&  (tableroJuego[1,1] == signo ) && (tableroJuego[2,0] == signo))
                         {
                            if (signo == 'X') 
                                System.Console.WriteLine("Felicitaciones Eres el ganador!! 1"); 
                            else
                                System.Console.WriteLine("Felicitaciones Eres el ganador!! 2");
                            
                            System.Console.WriteLine("Presione Cualquier tecla para reiniciar el juego");
                            Console.Read();
                            ingreso = 0;
                            Resetear();

                            break;
                             
                         }
                    else if( turnos == 10){

                        System.Console.WriteLine("\nEmpate!!");
                        System.Console.WriteLine("Presione Cualquier tecla para reiniciar el juego");
                        Console.Read();
                        ingreso = 0;
                        Resetear();
                        break;
                    }


                         

                }                
                    
                #endregion

                do{
                    System.Console.WriteLine("\nJugador {0}: Por favor elija un casillero...", jugador);
                    try
                    {
                        ingreso = Convert.ToInt32(Console.ReadLine());    
                        
                    }
                    catch 
                    {
                        System.Console.WriteLine("POR FAVOR INGRESE UN NUMERO");
                        
                    }
                    if ((ingreso == 1) && tableroJuego[0,0] == '1')
                        ingresoCorrecto = true;
                    else if ((ingreso == 2) && tableroJuego[0,1] == '2')
                        ingresoCorrecto = true;
                    else if ((ingreso == 3) && tableroJuego[0,2] == '3')
                        ingresoCorrecto = true;
                    else if ((ingreso == 4) && tableroJuego[1,0] == '4')
                        ingresoCorrecto = true;
                    else if ((ingreso == 5) && tableroJuego[1,1] == '5')
                        ingresoCorrecto = true;
                    else if ((ingreso == 6) && tableroJuego[1,2] == '6')
                        ingresoCorrecto = true;
                    else if ((ingreso == 7) && tableroJuego[2,0] == '7')
                        ingresoCorrecto = true;                   
                    else if ((ingreso == 8) && tableroJuego[2,1] == '8')
                        ingresoCorrecto = true;
                    else if ((ingreso == 9) && tableroJuego[2,2] == '9')
                        ingresoCorrecto = true;
                    else
                    {
                        System.Console.WriteLine("\nPor favor ingrese otro Número");
                        ingresoCorrecto = false; }
                      
                    
                } while (!ingresoCorrecto);
               
            } while (true);

        }

        // Array Numeros Tablero
        static char[,] tableroJuego = {
            {'1','2','3'},
            {'4','5','6'},
            {'7','8','9'}
        };

       static char[,] tableroJuegoInicial = {
            {'1','2','3'},
            {'4','5','6'},
            {'7','8','9'}
        };

        static int turnos = 0;
        // Método que Crea el tablero
        public static void CrearTablero(){

            Console.Clear();
            System.Console.WriteLine("     |     |");
            System.Console.WriteLine("  {0}  |  {1}  |  {2}",tableroJuego[0,0],tableroJuego[0,1],tableroJuego[0,2]);
            System.Console.WriteLine("_____|_____|_____");
            
            System.Console.WriteLine("     |     |");
            System.Console.WriteLine("  {0}  |  {1}  |  {2}",tableroJuego[1,0],tableroJuego[1,1],tableroJuego[1,2]);
            System.Console.WriteLine("_____|_____|_____");

            System.Console.WriteLine("     |     |");
            System.Console.WriteLine("  {0}  |  {1}  |  {2}",tableroJuego[2,0],tableroJuego[2,1],tableroJuego[2,2]);
            System.Console.WriteLine("     |     |");
            turnos++;

        }
        //Metodo reinicio

        public static void Resetear(){
            tableroJuego = tableroJuegoInicial;
            CrearTablero();
            turnos =0;
        }

        // Método para indetificar jugador
        public static void PonerXoO (int jugador, int ingreso){

            char signo = ' ';
            if (jugador == 1){

                signo = 'X';
            }
            else if(jugador == 2){

                signo = 'O';
            }

            switch (ingreso){

                case 1: tableroJuego[0,0] = signo; break ;
                case 2: tableroJuego[0,1] = signo; break ;
                case 3: tableroJuego[0,2] = signo; break ;
                case 4: tableroJuego[1,0] = signo; break ;
                case 5: tableroJuego[1,1] = signo; break ;
                case 6: tableroJuego[1,2] = signo; break ;
                case 7: tableroJuego[2,0] = signo; break ;
                case 8: tableroJuego[2,1] = signo; break ;
                case 9: tableroJuego[2,2] = signo; break ;
            }          
        }       
      }
}

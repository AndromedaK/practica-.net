using System;

namespace Practica_Basica_9
{
    public class Notificacion : INotificacion
    {
        // Variables

        private string enviadoPor;
        private string mensaje;
        private string fecha;

        public Notificacion()
        {
            enviadoPor  = "Fede";
            fecha       = "01.02.2020";
            mensaje     = "Hola Mundo";
        }

        public Notificacion(string enviadoPor, string mensaje, string fecha )
        {
            this.enviadoPor = enviadoPor;
            this.fecha      = fecha;
            this.mensaje    = mensaje;
        }

        // Hay que darle un valor de acceso a los metodos 
        public void MostrarNotificacion()
        {
            System.Console.WriteLine(" El mensaje: {0}, fue enviado por {1}, Fecha: {2}" , mensaje,enviadoPor,fecha);
        }

        public string VerFecha()
        {
            return fecha;
        }
    }
}
using System;

namespace Practica_Basica_9
{
    public interface INotificacion
    {
        void MostrarNotificacion();
        string VerFecha();
    }

}
﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Practica_Basica_11
{

    struct Juego
    {
        public string nombre;
        public string desarrollador;
        public Juego(string nombre, string desarrollador)
        {
            this.desarrollador = desarrollador;
            this.nombre        = nombre;
        }
        public void Mostrar()
        {
            System.Console.WriteLine("{0} {1}",nombre ,desarrollador);
        }
    }

    enum Dia{
        Lu, Ma, Mi, Ju, Vi, Sa, Do
    }

    enum Meses{
        enero = 1, febrero, marzo, abril, mayo, junio, julio, agosto, septiembre, octubre, noviembre, diciembre
    }

    public delegate int Calculos(int i);

  

    

    class Program
    {
        static void Main(string[] args)
        {

            #region Leer Archivo  
            /*
            string texto = System.IO.File.ReadAllText(@"C:\Users\Kurisu\Desktop\.Net\Practica-Basica-11\LeerArchivo.txt");
            System.Console.WriteLine("El archivo tiene el siguiente texto: {0}", texto);

            string [] lineas = System.IO.File.ReadAllLines(@"C:\Users\Kurisu\Desktop\.Net\Practica-Basica-11\LeerArchivo.txt");
            foreach (var linea in lineas)
            {
                System.Console.WriteLine("\t"+linea);
            }
            */
            #endregion

            #region structs
            /*
            Juego juego1;
            juego1.nombre = "resident evil";
            juego1.desarrollador = "Shinji";
            juego1.Mostrar();

            */
            #endregion

            #region Enums
            /*
            //Dia viernes = Dia.Vi;
            //Dia domingo = Dia.Do;

            System.Console.WriteLine((int)Dia.Lu);
            System.Console.WriteLine(Dia.Lu);

            System.Console.WriteLine((int)Meses.enero);
            System.Console.WriteLine((int)Meses.febrero);
            */        
                
            #endregion
        
            #region Math
                /*
                int a = 13;
                int b = 9;
                
                System.Console.WriteLine("Redondeo hacia arriba de 15.3 es {0}" , Math.Ceiling(15.3));
                System.Console.WriteLine("Redondeo hacia abajo de 15.3 es {0}" , Math.Floor(15.3));
                System.Console.WriteLine("El valor mas bajo entre {0} y {1} es: {2}", a,b, Math.Min(a,b));
                System.Console.WriteLine("El valor mas Alto entre {0} y {1} es: {2}", a,b, Math.Max(a,b));
                System.Console.WriteLine("3 elevado a 5 es: " + Math.Pow(3,5));
                System.Console.WriteLine("Pi es: {0}" , Math.PI);
                System.Console.WriteLine("La Raiz cuadrdo de 25 es  {0}", Math.Sqrt(25));
                System.Console.WriteLine("El valor aboluto de -25 es {0}", Math.Abs(-25));
                System.Console.WriteLine("El coseno de 1 es {0}", Math.Cos(1) );

                */


            #endregion
     
            #region Random

            /*
            Random dado = new Random();
            int numCara;

            for (int i = 0; i < 10; i++)
            {
                numCara = dado.Next(1,7);
                System.Console.WriteLine(numCara);
            }

            System.Console.WriteLine("Haz un pregunta");
            Random ruedaFortuna = new Random();
            int numRespuesta = ruedaFortuna.Next(1,4);

            if (numRespuesta == 1)
            {
                System.Console.WriteLine("Si");
            }   
            else if( numRespuesta == 2)
            {
                System.Console.WriteLine("Quizas");
            } 
            else if( numRespuesta == 3)
            {
                System.Console.WriteLine("No");
            }
             */
            #endregion
       
            #region Expresiones Regulares

            /*
            string patron = @"\d";
            string texto  = "Hola, mi numero telefonico es 1234567";
            Regex regex = new Regex(patron);

            MatchCollection aciertos = regex.Matches(texto);
            System.Console.WriteLine(aciertos.Count);

            foreach (Match acierto in aciertos)
            {
                GroupCollection grupo= acierto.Groups;
                System.Console.WriteLine("{0} fue encontrado en {1}",grupo[0].Value, grupo[0].Index);               
            }
            */
            #endregion
       
            #region Datetime

            DateTime tiempo = new DateTime(2020,3,4);
            System.Console.WriteLine(tiempo);

            // Mostrar fecha actual
            System.Console.WriteLine(DateTime.Today);

            // Hora Actual 
            System.Console.WriteLine(DateTime.Now);

            // Feche de mañana 
            DateTime tomorrow = ProximoDia();
            System.Console.WriteLine("Mañana va a ser {0}",tomorrow);

            // Dia de la semana 
            System.Console.WriteLine(DayWeek());

            // Primer dia del año
            System.Console.WriteLine(PrimerDia(1998));

            // Cantidad de dias en el mes de tal año 
            int dias = DateTime.DaysInMonth(2000,2);
            System.Console.WriteLine(dias);

            // x horas, minutos, segundos
            DateTime ahora = DateTime.Now;
            System.Console.WriteLine("{0}:{1}:{2}:{3}",ahora.Hour, ahora.Minute, ahora.Second, ahora.Millisecond);

            //System.Console.WriteLine("Porfavor ingrese una fecha en el siguiente formato: aaaa-mm-dd");
            string ingreso = "1998-04-20";
            if (DateTime.TryParse(ingreso, out tiempo))
            {
                System.Console.WriteLine(tiempo);
                TimeSpan diasEnteros = ahora.Subtract(tiempo);
                System.Console.WriteLine("Dias que pasaron de esa fecha: {0}:" , diasEnteros.Days);
            }
            else
            {
                System.Console.WriteLine("Ingreso Incorrecto");
            }

            #endregion

            #region Nulabilidad

                // El signo de interrogacion admite los nulls 
                int? num1 = null;
                int? num2 = 12;
                double? num3 = new double?();
                double? num4 = 3.14157;

                bool? valor = new bool?();

                bool? esHombre = null;

                if (esHombre == true)
                {
                    System.Console.WriteLine("Es hombre");
                }
                else if( esHombre == false)
                {
                    System.Console.WriteLine("es mujer");
                }
                else
                {
                    System.Console.WriteLine("Indeterminado");
                }

                



            #endregion 

            #region Expresiones Lambda

            HacerAlgo();
            #endregion
        }

        static DateTime ProximoDia()
        {
            return DateTime.Today.AddDays(1);
        }

        static DayOfWeek DayWeek()
        {   
           return DateTime.Today.DayOfWeek;
        }

        static DateTime PrimerDia(int anio)
        {
            return new DateTime(anio,1,1);
        }

        public static void HacerAlgo()
        {
            Calculos mate = new Calculos(cuadrado);
            System.Console.WriteLine(mate(8));

            List<int> lista = new List<int>{1,2,3,4,5,6,7};
            List<int> numeroPares = lista.FindAll(delegate(int i)
            {
                return ( i % 2 ==0);
            });
            foreach (var par in numeroPares)
            {
                System.Console.WriteLine(par);
            }

            List<int> ListaImparesInicial = new List<int>{1,3,5,7,9,2,4,6,8,10,11,12,14,13};
            List<int> ListaImparesFiltrada =  ListaImparesInicial.FindAll(delegate(int i)
            {
                return ( i % 2 == 1);
            });

            foreach (var impar in ListaImparesFiltrada)
            {
                System.Console.WriteLine(impar);
            }

            var ListaImparesFiltradaLambda = ListaImparesInicial.FindAll( x =>  x % 2 == 1 );
            ListaImparesFiltradaLambda.ForEach( y =>
            {
                 System.Console.WriteLine("Numero impar:");
                 System.Console.WriteLine(y);
            }
            );

            mate = new Calculos(x =>  x* x* x );
            System.Console.WriteLine(mate(8));



        }

        public static int sumar (int a , int b)
        {
            return a + b;
        }

        public static int cuadrado(int i)
        {
            return i * i;
        }
   
    }
}

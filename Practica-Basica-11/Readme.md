# Modificadores de Acceso

- Permiten o Impiden el acceso

## Caracteristicas 

1. Crear Campos y Metodos con modificadores de acceso es parte de las POO 
2. Incremetan la seguridad del codigo 
3. Es una parte importante de la encapsulacion 

    * Un mecanismo Linguistico para restringir el acceso a alguno de los componentes del objeto
    * Un constructo del lenguaje que facilita la agrupacion de informacion con los metodos que operan en esa informacio

# Privado

###  Private: Solo es accesible dentro de la clase 

```c#
    
    public class Clase1{

        private int edad = 18;

        private void Caminar()
        {

        }   
    }

    public class Clase2{

        Clase1 primeraClase = new Clase1();
        int i = primeraClase.edad; // ERROR
        primeraClase.Caminar(); // ERROR

        // EDAD Y CAMINAR() NO SO ACCESIABLES DESDE OTRA CLASE 
    }
```
# Publico

###  Public: es accesible desde cualquier lugar del proyecto

```c#
    
    public class Clase1{

        public int edad = 18;

        public void Caminar()
        {

        }   
    }

    public class Clase2{

        Clase1 primeraClase = new Clase1();
        int i = primeraClase.edad; // OK
        primeraClase.Caminar(); // OK

        // EDAD Y CAMINAR() SON ACCESIBLES DESDE CUALQUIER LUGAR
    }
```

# Protegido

###  Protected: Accesible dentro de la clase y sus clases derivadas

```c#
    
    public class Clase1{

        protected int edad = 18;

        protected void Caminar()
        {

        }   
    }

    public class Clase2: Clase1
    {

        int i = edad; // OK
        Caminar(); // OK

        // EDAD Y CAMINAR() SON ACCESIBLES PORQUE ES UNA CLASE DERIVADA
    }
```
# Interno

###  Internal: Accesible desde todo el ensamble (Proyecto)

```c#
    
    public class Clase1{

        internal int edad = 18;

        internal void Caminar()
        {

        }   
    }
```

# Structs 


1. No se pueden crear constructores por defecto
2. Los struct son de tipo valor y necesitan tener un valor especifico para poder funcionar
3. Se pueden implementar interfaces
4. No soportan herencia 
5. Se utilizan constructores parametrizados 

# Enums

1. Un grupo de constantes inmutables
2. debe ubicarse a nivel del namespace 
3. Siempre tienen el mismo valor 



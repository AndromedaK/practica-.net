using System;

namespace Practica_Basica_10
{
    public  class BMW: Auto
    {
        public string Modelo { get; set; }
        private string marca = "BMW";


        public BMW(string modelo, string color, int hp):base(hp, color)
        {
            this.Modelo = modelo;
        }

        public override void Reparar()
        {

           System.Console.WriteLine("{0} Reparado", marca);

        }

        // Metodo New : Ocultar un metodo de la clase base para que tenga prioridad al de la clase derivada
        // Queremos que el metodo de la clase base aparazca por mas que exista otro metodo en la clase derivada 
        // Señalamos prioridad, no sobreescribe 
        public new void MostrarDetalles()
        {

            System.Console.WriteLine("HP: {0}; Color:{1}; Marca:{2} ; Modelo:{3} ",this.HP, this.Color, this.marca,this.Modelo);

        }



    }
    
}
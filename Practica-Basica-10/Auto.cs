using System;

namespace Practica_Basica_10
{

    public class Auto
    {
        protected int HP { get; set; }
        protected string Color { get; set; }

        // Generar una relacion con AutoInfo
        protected AutoInfo autoInfo = new AutoInfo();  
        public Auto(int hp, string color)
        {
            this.HP    = hp;
            this.Color = color;            
        }

        public void MostrarDetalles()
        {
            System.Console.WriteLine("HP: {0}; Color:{1} ",this.HP, this.Color);
        }

        public virtual void Reparar()
        {
            System.Console.WriteLine("Auto Reparado");
        }

        public void SetearAutoinfo(int id, string propietario)
        {
            autoInfo.ID = id;
            autoInfo.Propietario = propietario;

        }

        public void LeerAutoInfo()
        {
            System.Console.WriteLine("ID: {0}; Propietario {1}",autoInfo.ID,autoInfo.Propietario);
        }
  
    }
    
}
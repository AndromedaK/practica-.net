﻿using System;
using System.Collections.Generic;

namespace Practica_Basica_10
{
    class Program
    {
        static void Main(string[] args)
        {
            var ListAuto  = new List<Auto>
            {
               new Audi("a4",200,"Azul"),
               new BMW("a9","Rojo", 120)   
            };

            foreach (var auto in ListAuto)
            {
                auto.Reparar();
            }

            // Declarar Objetos - Podemos utilizar una subclase de la clase Auto para crear un objeto nuevo
            Auto auto1 = new BMW("z9","blanco", 120);
            Auto auto2 = new Audi("z10", 120,"negro");

            auto1.SetearAutoinfo(1000,"Patricio Rios");
            auto2.SetearAutoinfo(2000,"Juan Rios");
            auto1.LeerAutoInfo();
            auto2.LeerAutoInfo();

            // PRIORIDAD METODO CLASE BASE AUTO 
            auto1.MostrarDetalles();
            auto2.MostrarDetalles();

            BMW bmwM5  = new BMW("M5","Verde",700);
            bmwM5.MostrarDetalles();

            // Casting de Tipo Auto al objeto creado anteriormente bmwM5 
            Auto bmwM5TipoAuto = (Auto)bmwM5;
            bmwM5TipoAuto.MostrarDetalles();

            M3 miM3  = new M3(230,"Azul","M3 Super Turbo");
            miM3.Reparar();

        }
    }
}

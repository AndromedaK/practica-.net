using System;

namespace Practica_Basica_10
{
    public class Audi : Auto
    {
        public string Modelo { get; set; }
        private string marca = "Audi";

        public Audi(string modelo,  int hp, string color):base(hp, color)
        {
            this.Modelo = modelo;
        }
        public override void Reparar()
        {
           System.Console.WriteLine("{0} Reparado", marca);
        }

        public new  void MostrarDetalles()
        {
            System.Console.WriteLine("HP: {0}; Color:{1}; Marca:{2} ; Modelo:{3} ",this.HP, this.Color, this.marca,this.Modelo);
        }





    }


}
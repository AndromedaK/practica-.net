﻿using System;

namespace Practica_Basica_4
{
    class Program
    {
        static void Main(string[] args)
        {
             
            #region Primera Manera para crear Array
                /*
            int[] calificaciones  = new int[5];
            calificaciones[0] = 1;
            calificaciones[1] = 7;
            calificaciones[2] = 6;
            calificaciones[3] = 7;
            calificaciones[4] = 7;

            string ingreso = Console.ReadLine();
            calificaciones[4] = int.Parse(ingreso);
            System.Console.WriteLine("");
            System.Console.WriteLine("Primera Calificacion {0}", calificaciones[4]);               
                
                
                */
            #endregion

            // Segunda Manera para crear Array
            int[] edades = {1,2,2,3};
            // Tercera Manera para crear Array
            int [] faltas = new int[]{3,5,6,7,8};

            int[] numeros = new int[10];

            for(int i = 0; i < numeros.Length ; i++ ){

                numeros[i] = i ;
            }
            
            /*
            for(int j = 0; j < numeros.Length; j++){

                System.Console.WriteLine("El valor del indice {0} es {1}", j , numeros[j]);
            }*/

            foreach (var k in numeros)
            {
                Console.WriteLine("El valor del indice {0} es {1}", k , numeros[k]);
            }

            string[] amigos = new string[5]{"rodrigo","Diego","Mieguek","Huab","Pedro"};
            for (int i = 0; i < amigos.Length; i++)
            {
                   Console.WriteLine("Amigo {0}: {1}", i, amigos[i]);
                                   
            }

            foreach (string item in amigos)
            {
                    System.Console.WriteLine("Hola Amigo {0}", item);
            }
 
            #region Array Multidimensionales
                
                // declaracion de array 2d 
                string [,] matriz2d;

                // declaracion de array 3d
                string[,,] matriz3d;

                // Filas por columnas
                string[,] array2D = new string[,]{ 

                    {"a","b","c"},
                    {"d","e","f"},
                    {"g","h","i"}
                };


                System.Console.WriteLine("El valor central es {0}", array2D[1,1]);
                System.Console.WriteLine("El valor es {0}", array2D[2,0]);

                string[,,] array3D = new string[,,]{
                    {
                        {"a","b","c"},
                        {"d","e","f"},
                        {"g","h","i"}
                    },
                                        {
                        {"j","k","l"},
                        {"m","n","o"},
                        {"p","q","r"}
                    },
                                        {
                        {"s","t","u"},
                        {"v","w","x"},
                        {"y","z","*"}
                    }

                };
                // Numero de tabla, Numero de fila, Numero de columna 
                System.Console.WriteLine("El valor A es {0}", array3D[0,0,0]);
                System.Console.WriteLine("El valor de R es {0}", array3D[1,2,2]);
                // Getting the total count of elements or the length of a given dimension.
        
                string[,] otroArray2D = new string[3,2]{
                    {"Nombre", "Cristopher"},
                    {"Nombre", "Cristopher"},
                    {"Nombre", "Cristopher"}                   
                    };

                int dimensiones = otroArray2D.Rank;
                System.Console.WriteLine("Las dimensiones del array son: {0}",dimensiones);

                int[,] a2D = { {1,2}, {3,2}, {4,5}};

            #endregion



        }
    }
}

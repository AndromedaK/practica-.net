# Arrays

1. Almacenan una cantidad fija de elementos
2. Solo elementos del mismo tipo entre sí
3. Elementos de cualquier tipo: strings, int, bool, etc
4. Ideal para almacenar una colección de datos


## Declaracion
```c# 
 // example
 // TipoDeDato [] nombreDelArray;
 # Declarar un array
    int [] calificaciones;
 # Inicializar un array 
    int [] calificaciones = new int [5];
 # Asignar valores a un Array
    calificaciones [0] = 7;
    calificaciones [1] = 9;

```
using System;

namespace Practica_Basica_8
{

    public class Jefe: Empleado
    {
        public string AutoDeEmpresa { get; set; }

        public Jefe()
        {
            
        }
        public Jefe(string marca,string nombre, string apellido, decimal salario)
        {
            this.Apellido = apellido;
            this.Nombre   = nombre;
            this.Salario  = salario; 
            this.AutoDeEmpresa = marca;
        }

        public void Dirigir(){
            System.Console.WriteLine("Soy el Jefe");
        }

    }

}
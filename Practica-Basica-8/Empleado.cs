using System;

namespace Practica_Basica_8
{
    public class Empleado
    {

        protected string  Nombre { get; set; }
        protected string  Apellido { get; set; }
        protected decimal  Salario { get; set; }

        public Empleado()
        {
            
        }
        public Empleado(string nombre, string apellido, decimal salario)
        {
            this.Apellido = apellido;
            this.Nombre   = nombre;
            this.Salario  = salario; 
        }

        public virtual void Trabajar(){
            System.Console.WriteLine("Esyot Trabajando");
        }
        public void Descansar(){
            System.Console.WriteLine("Estoy Descansando");
        }
        

    }
    
}
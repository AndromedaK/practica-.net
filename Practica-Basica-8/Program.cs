﻿using System;

namespace Practica_Basica_8
{
    class Program
    {
        static void Main(string[] args)
        {
           
           Empleado empleado1 = new Empleado("Cristopher","Angulo", 1500);
           Jefe     jefe1     = new Jefe("Subaru","Joaquin","Jara", 4000);
           Pasante  pasante1  = new Pasante(8,3,"Javier","hernandez",500);

           jefe1.Dirigir();
           pasante1.Trabajar();


        }
    }
}

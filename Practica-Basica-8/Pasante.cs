using System;

namespace Practica_Basica_8
{

    public class Pasante: Empleado
    {
        public int HorasDeTrabajo { get; set; }
        public int HorasDeEscuela { get; set; }

        public Pasante()
        {
            
        }

        public Pasante(int HorasDeTrabajo, int HorasDeEscuela, string nombre, string apellido, decimal salario)
        {
            this.HorasDeEscuela = HorasDeEscuela;
            this.HorasDeTrabajo = HorasDeTrabajo;
            this.Apellido = apellido;
            this.Nombre   = nombre;
            this.Salario  = salario; 
        }

        public void Aprender()
        {
            System.Console.WriteLine("Estoy Aprendiendo durante {0} cantidad de horas", this.HorasDeEscuela);
        }

        public override void Trabajar(){
            System.Console.WriteLine("El pasante trabajo {0} horas semanales", HorasDeTrabajo);
        }

    }

}
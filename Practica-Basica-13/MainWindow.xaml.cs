﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Practica_Basica_13
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // Propiedad comun
        public int EyesColor
        {
            get { return (int)GetValue(EyesColorProperty); }
            
            set { SetValue(EyesColorProperty, value); }
        
        }

        public string Nombre
        {
            get
            {
                return (string)GetValue(NombreDependencyProperty);
            }
            set 
            {
                SetValue(NombreDependencyProperty, value);
            } 
        }


        // propiedad de dependencia 
        public static readonly DependencyProperty EyesColorProperty =
            DependencyProperty.Register("EyesColor", typeof(int), typeof(MainWindow), new PropertyMetadata(0));


        public static readonly DependencyProperty NombreDependencyProperty =
            DependencyProperty.Register("Nombre", typeof(string), typeof(MainWindow), new PropertyMetadata(0));

        public MainWindow()
        {
            InitializeComponent();
            
        }
    }
}
